#!/usr/bin/env bash
SCRIPT_PATH=$(dirname "$(realpath $0)");
GBDK_HOME=$(realpath "$SCRIPT_PATH/..");


INPUT_FILE=$4;
OUTPUT_FILE=$2;

$GBDK_HOME/bin/sdcc                                                            \
  -mgbz80                                                                      \
   --no-std-crt0                                                               \
   --fsigned-char                                                              \
   --use-stdout                                                                \
   -Wa-pog                                                                     \
   -DGB=1                                                                      \
   -DGAMEBOY=1                                                                 \
   -DINT_16_BITS                                                               \
   -D__LCC__                                                                   \
   -I"$GBDK_HOME/include"                                                      \
   -c $INPUT_FILE -o $OUTPUT_FILE

echo "Building: ($OUTPUT_FILE)"
